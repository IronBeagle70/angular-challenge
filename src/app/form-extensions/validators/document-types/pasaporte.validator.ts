import { AbstractControl, ValidatorFn } from '@angular/forms';

export const passportMustBevalid: ValidatorFn = (control: AbstractControl) => {
  
  const passportValue = control.value;
  
  const passportRegex = new RegExp(/^[0-9]*$/);
  const passaportPattern = passportRegex.test(passportValue);
  
  if (!passportValue) {
    return { required: true };
  }

  if (!passaportPattern) {
    return { passportMustBevalid: true };
  }

  if((passportValue.length > 0 && passportValue.length < 9) || passportValue.length > 9 ){
    return { passportMustBevalid: true };
  }

  return null;
};
