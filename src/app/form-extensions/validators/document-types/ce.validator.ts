import { AbstractControl, ValidatorFn } from '@angular/forms';

export const ceMustBeValid: ValidatorFn = (control: AbstractControl) => {
  
  const ceValue = control.value;
  
  const ceRegex = new RegExp(/^[a-zA-Z0-9]*$/);
  const cePattern = ceRegex.test(ceValue);
  
  if (!ceValue) {
    return { required: true };
  }

  if (!cePattern) {
    return { ceMustBeValid: true };
  }

  if((ceValue.length > 0 && ceValue.length < 9) || ceValue.length > 9 ){
    return { ceMustBeValid: true };
  }

  return null;
};
