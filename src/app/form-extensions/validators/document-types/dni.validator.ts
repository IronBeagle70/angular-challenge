import { AbstractControl, ValidatorFn } from '@angular/forms';

export const dniMustBeValid: ValidatorFn = (control: AbstractControl) => {
  
  const dniValue = control.value;
  
  const dniRegex = new RegExp(/^[0-9]*$/);
  const dniPattern = dniRegex.test(dniValue);
  
  if (!dniValue) {
    return { required: true };
  }

  if (!dniPattern) {
    return { dniMustBeValid: true };
  }

  if((dniValue.length > 0 && dniValue.length < 8) || dniValue.length > 8 ){
    return { dniMustBeValid: true };
  }

  return null;
};
