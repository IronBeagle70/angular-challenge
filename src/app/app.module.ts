import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { NumberMessagePipe } from './pipes/number-message/number-message.pipe';
import { BtnMessagePipe } from './pipes/button-message/btn-message.pipe';

import { AppComponent } from './app.component';
import { RegisterFormComponent } from './components/register-form/register-form.component';
import { UpdateFormComponent } from './components/update-form/update-form.component';
import { PassengersComponent } from './components/passengers/passengers.component';
import { PassengerComponent } from './components/passenger/passenger.component';
import { SidenavComponent } from './components/sidenav/sidenav.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { RegisterComponent } from './pages/register/register.component';
import { SummaryComponent } from './pages/summary/summary.component';

import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatDividerModule } from '@angular/material/divider';

import { FormExtensionsModule } from './form-extensions/form-extesions.module';
import { NotificationsModule } from './notifications/notifications.module';
import { ConfirmDialogModule } from './confirm-dialog/confirm-dialog.module';

@NgModule({
  declarations: [
    AppComponent,
    BtnMessagePipe,
    NumberMessagePipe,
    SidenavComponent,
    ToolbarComponent,
    RegisterFormComponent,
    UpdateFormComponent,
    PassengersComponent,
    PassengerComponent,
    RegisterComponent,
    SummaryComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    BrowserAnimationsModule,
    AppRoutingModule,
    FormExtensionsModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    MatButtonModule,
    MatToolbarModule,
    MatSidenavModule,
    MatDividerModule,
    NotificationsModule,
    ConfirmDialogModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
