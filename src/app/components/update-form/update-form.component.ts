import { Component, Input, OnInit } from '@angular/core';

import { FormControl, FormGroup } from '@angular/forms';

import { Passenger } from 'src/app/models/passenger.model';

import { namesMustbeValid } from 'src/app/form-extensions/validators/names.validator';
import { surnamesMustbeValid } from 'src/app/form-extensions/validators/surnames.validator';
import { nationalityMustbeValid } from 'src/app/form-extensions/validators/nationality.validator';

import { NotifierService } from 'src/app/services/notifications/notifier.service';
import { UpdateService } from 'src/app/services/updateForm/update.service';
import { ValidatorsService } from 'src/app/services/validators/validators.service';

@Component({
  selector: 'app-update-form',
  templateUrl: './update-form.component.html',
  styleUrls: ['./update-form.component.scss'],
})
export class UpdateFormComponent implements OnInit {
  @Input() passenger!: Passenger;

  documentType = ['DNI', 'CE', 'Pasaporte'];

  updateForm = new FormGroup({
    newNames: new FormControl('', [namesMustbeValid]),
    newSurnames: new FormControl('', [surnamesMustbeValid]),
    newNationality: new FormControl('', [nationalityMustbeValid]),
    newDocumentType: new FormControl(),
    newDocumentNumber: new FormControl(),
  });

  constructor(
    private updateService: UpdateService,
    private notifierService: NotifierService,
    private validatorsService: ValidatorsService
  ) {}

  ngOnInit() {
    this.updateForm.patchValue({
      newNames: this.passenger.names,
      newSurnames: this.passenger.surnames,
      newNationality: this.passenger.nationality,
      newDocumentType: this.passenger.documentType,
      newDocumentNumber: this.passenger.documentNumber,
    });

    this.newDocumentTypeControl?.valueChanges.subscribe((value) => {
      this.validatorsService.setupDocumentNumberValidators(
        this.newDocumentNumberControl,
        value
      );
    });
  }

  get newNamesControl(): FormControl {
    return this.updateForm.get('newNames') as FormControl;
  }

  get newSurnames(): FormControl {
    return this.updateForm.get('newSurnames') as FormControl;
  }

  get newNationality(): FormControl {
    return this.updateForm.get('newNationality') as FormControl;
  }

  get newDocumentTypeControl(): FormControl {
    return this.updateForm.get('newDocumentType') as FormControl;
  }

  get newDocumentNumberControl(): FormControl {
    return this.updateForm.get('newDocumentNumber') as FormControl;
  }

  onUpdatePassenger() {
    const changes = this.updateForm.value;
    this.passenger.names = changes.newNames || '';
    this.passenger.surnames = changes.newSurnames || '';
    this.passenger.nationality = changes.newNationality || '';
    this.passenger.documentType = changes.newDocumentType || '';
    this.passenger.documentNumber = changes.newDocumentNumber || '';
    this.onClose();
    this.notifierService.showNotification(
      'Pasajero Actualizado Satisfactoriamente',
      'Ok'
    );
  }

  onClose() {
    this.updateService.setIsEditing(false);
  }
}
