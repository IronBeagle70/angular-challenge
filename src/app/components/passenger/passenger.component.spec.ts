import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { By } from '@angular/platform-browser';
import { of } from 'rxjs';

import { Passenger } from 'src/app/models/passenger.model';

import { NotifierService } from 'src/app/services/notifications/notifier.service';
import { StoreService } from 'src/app/services/store/store.service';

import { UpdateFormComponent } from '../update-form/update-form.component';
import { PassengerComponent } from './passenger.component';

describe('PassengerComponent', () => {
  let component: PassengerComponent;
  let fixture: ComponentFixture<PassengerComponent>;
  let el: DebugElement;
  let notifierService: NotifierService;
  let storeService: StoreService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MatSnackBarModule, 
        MatIconModule, 
        MatDialogModule
      ],
      declarations: [
        PassengerComponent, 
        UpdateFormComponent
      ],
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(PassengerComponent);
        component = fixture.componentInstance;
        el = fixture.debugElement;
        notifierService = TestBed.inject(NotifierService);
        storeService = TestBed.inject(StoreService);
      });
  });

  it('passenger component should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should display passenger containers', () => {
    const passengerContainer = el.nativeElement.querySelectorAll('.passenger');
    expect(passengerContainer.length).toBe(1);

    const passengerData = el.nativeElement.querySelectorAll('.passenger div');
    expect(passengerData.length).toBe(5);
  });

  it('should display passenger data', () => {
    const passenger: Passenger = {
      names: 'names test',
      surnames: 'surnames test',
      nationality: 'nationality test',
      documentType: 'DNI',
      documentNumber: '321',
    };
    component.passenger = passenger;
    fixture.detectChanges();

    const dataContainer = el.queryAll(By.css('.passenger__data'));
    expect(dataContainer.length).toBe(4);

    const span = el.queryAll(By.css('span'));
    expect(span[0].nativeElement.textContent).toContain(
      `${passenger.names.toUpperCase()} ${passenger.surnames.toUpperCase()} `
    );
    expect(span[1].nativeElement.textContent).toContain(
      passenger.nationality.toUpperCase()
    );
    expect(span[2].nativeElement.textContent).toContain(
      passenger.documentType.toUpperCase()
    );
    expect(span[3].nativeElement.textContent).toContain(
      passenger.documentNumber.toUpperCase()
    );
  });

  it('should display passenger icons', () => {
    const icon = el.nativeElement.querySelectorAll('mat-icon');
    expect(icon.length).toBe(4);

    expect(icon[0].textContent).toEqual('person');
    expect(icon[1].textContent).toEqual('public');
    expect(icon[2].textContent).toEqual('badge');
    expect(icon[3].textContent).toEqual('tag');
  });

  it('should display update and delete buttons', () => {
    const btnElements = el.nativeElement.querySelectorAll(
      '.btn__container button'
    );
    expect(btnElements.length).toBe(2);

    expect(btnElements[0].textContent).toEqual('Actualizar');
    expect(btnElements[1].textContent).toEqual('Eliminar');
  });

  it('should call onEditPassenger when edit button is clicked', () => {
    spyOn(component, 'onEditPassenger');

    const editButton = el.queryAll(By.css('button'))[0];
    editButton.triggerEventHandler('click', null);

    expect(component.onEditPassenger).toHaveBeenCalled();
  });

  it('should call onDeletePassenger when delete button is clicked', () => {
    const index = storeService.getPassengers().indexOf(component.passenger);

    spyOn(component, 'onDeletePassenger').and.callThrough();
    const spyConfirmDialog = spyOn(
      notifierService,
      'showConfirmDialog'
    ).and.returnValue(of(true));

    const spyRemovePassenger = spyOn(storeService, 'removePassenger');

    const deleteButton = el.queryAll(By.css('button'))[1];
    deleteButton.triggerEventHandler('click', null);

    expect(component.onDeletePassenger).toHaveBeenCalled();
    expect(spyConfirmDialog).toHaveBeenCalledWith(
      'Eliminar Pasajero',
      '¿Desea eliminar los datos de este pasajero?'
    );
    expect(spyRemovePassenger).toHaveBeenCalledWith(index);
  });
});
