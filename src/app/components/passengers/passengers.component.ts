import { Component, Input } from '@angular/core';

import { Passenger } from 'src/app/models/passenger.model';

@Component({
  selector: 'app-passengers',
  templateUrl: './passengers.component.html',
  styleUrls: ['./passengers.component.scss']
})
export class PassengersComponent {
  @Input() passengers!: Passenger[];
}
