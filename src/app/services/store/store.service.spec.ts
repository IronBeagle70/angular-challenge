import { TestBed } from '@angular/core/testing';

import { MatSnackBar } from '@angular/material/snack-bar';

import { Passenger } from 'src/app/models/passenger.model';

import { NotifierService } from '../notifications/notifier.service';
import { StoreService } from './store.service';

describe('StoreService', () => {
  let service: StoreService;
  let matSnackBarSpy: jasmine.Spy;
  let notifierServiceSpy: jasmine.SpyObj<NotifierService>;

  beforeEach(() => {
    notifierServiceSpy = jasmine.createSpyObj('NotifierService', [
      'showNotification',
    ]);
    TestBed.configureTestingModule({
      providers: [
        { provide: NotifierService, useValue: notifierServiceSpy },
        { provide: MatSnackBar, useValue: matSnackBarSpy },
      ],
    });
    service = TestBed.inject(StoreService);
  });

  it('store service should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should add passenger correctly', () => {
    const passenger: Passenger = {
      names: 'names test',
      surnames: 'surnames test',
      nationality: 'nationality test',
      documentType: 'documentType test',
      documentNumber: 'documentNumber test',
    };
    service.addPassenger(passenger);
    service.passengers$.subscribe((passengers) => {
      expect(passengers.length).toEqual(1);
      expect(passengers[0]).toEqual(passenger);
    });
    expect(notifierServiceSpy.showNotification).toHaveBeenCalledWith(
      'Pasajero Registrado Satisfactoriamente',
      'Ok'
    );
  });

  it('should not add more than four passengers', () => {
    const passenger: Passenger = {
      names: 'names test',
      surnames: 'surnames test',
      nationality: 'nationality test',
      documentType: 'documentType test',
      documentNumber: 'documentNumber test',
    };
    service.addPassenger(passenger);
    service.addPassenger(passenger);
    service.addPassenger(passenger);
    service.addPassenger(passenger);
    service.addPassenger(passenger);
    service.passengers$.subscribe((passengers) => {
      expect(passengers.length).toEqual(4);
    });
    expect(notifierServiceSpy.showNotification).toHaveBeenCalledWith(
      'No se puede registrar más de cuatro pasajeros',
      'Ok'
    );
  });

  it('should remove passenger correctly', () => {
    const passenger: Passenger = {
      names: 'names test',
      surnames: 'surnames test',
      nationality: 'nationality test',
      documentType: 'documentType test',
      documentNumber: 'documentNumber test',
    };
    service.addPassenger(passenger);
    service.addPassenger(passenger);
    service.removePassenger(1);
    service.passengers$.subscribe((passengers) => {
      expect(passengers.length).toEqual(1);
      expect(passengers[0]).toEqual(passenger);
    });
    expect(notifierServiceSpy.showNotification).toHaveBeenCalledWith(
      'Pasajero Eliminado Satisfactoriamente',
      'Ok'
    );
  });

  it('should return correct passengers', () => {
    const passenger1: Passenger = {
      names: 'names test1',
      surnames: 'surnames test1',
      nationality: 'nationality test1',
      documentType: 'documentType test1',
      documentNumber: 'documentNumber test1',
    };
    const passenger2: Passenger = {
      names: 'names test2',
      surnames: 'surnames test2',
      nationality: 'nationality test2',
      documentType: 'documentType test2',
      documentNumber: 'documentNumber test2',
    };
    service.addPassenger(passenger1);
    service.addPassenger(passenger2);
    const passengers = service.getPassengers();
    expect(passengers.length).toEqual(2);
    expect(passengers[0]).toEqual(passenger1);
    expect(passengers[1]).toEqual(passenger2);
  });
});
