import { TestBed } from '@angular/core/testing';

import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

import { DialogComponent } from 'src/app/confirm-dialog/components/dialog/dialog.component';
import { NotifierComponent } from 'src/app/notifications/components/notifier/notifier.component';

import { NotifierService } from './notifier.service';

import { of } from 'rxjs';

describe('NotifierService', () => {
  let notifierService: NotifierService;
  let matSnackBar: MatSnackBar;
  let matSnackBarSpy: jasmine.Spy;
  let matDialog: MatDialog;
  let matDialogSpy: jasmine.SpyObj<MatDialog>;

  beforeEach(() => {
    matSnackBarSpy = jasmine.createSpyObj('MatSnackBar', ['openFromComponent']);
    matDialogSpy = jasmine.createSpyObj('MatDialog', ['open']);
    TestBed.configureTestingModule({
      providers: [
        NotifierService,
        { provide: MatSnackBar, useValue: matSnackBarSpy },
        { provide: MatDialog, useValue: matDialogSpy },
      ],
    });
    notifierService = TestBed.inject(NotifierService);
    matSnackBar = TestBed.inject(MatSnackBar);
    matDialog = TestBed.inject(MatDialog);
  });

  it('notifier service should be created', () => {
    expect(notifierService).toBeTruthy();
  });

  it('should call openFromComponent method of MatSnackBar', () => {
    const displayMessage = 'Test message';
    const buttonText = 'Test button text';
    notifierService.showNotification(displayMessage, buttonText);
    expect(matSnackBar.openFromComponent).toHaveBeenCalledWith(
      NotifierComponent,
      jasmine.objectContaining({
        duration: 5000,
        data: { message: displayMessage, buttonText: buttonText },
        horizontalPosition: 'center',
        verticalPosition: 'bottom',
      })
    );
  });

  it('should create a confirm dialog', () => {
    const titleMessage = 'Test title dialog';
    const displayMessage = 'Test message dialog';

    const dialogRefSpyObj = jasmine.createSpyObj({ afterClosed : of({}) });
    matDialogSpy.open.and.returnValue(dialogRefSpyObj);
    
    notifierService.showConfirmDialog(titleMessage, displayMessage);
    expect(matDialog.open).toHaveBeenCalledWith(
      DialogComponent,
      jasmine.objectContaining({
        data: { title: titleMessage, message: displayMessage },
      })
    );
    expect(dialogRefSpyObj.afterClosed).toHaveBeenCalled();
  });
});
