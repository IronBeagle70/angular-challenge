import { Injectable } from '@angular/core';

import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

import { Observable } from 'rxjs';

import { DialogComponent } from 'src/app/confirm-dialog/components/dialog/dialog.component';
import { NotifierComponent } from 'src/app/notifications/components/notifier/notifier.component';

@Injectable({
  providedIn: 'root',
})
export class NotifierService {
  constructor(private snackBar: MatSnackBar, private dialog: MatDialog) {}

  showNotification(displayMessage: string, buttonText: string) {
    this.snackBar.openFromComponent(NotifierComponent, {
      duration: 5000,
      data: {
        message: displayMessage,
        buttonText: buttonText,
      },
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
    });
  }

  showConfirmDialog(titleMessage: string, displayMessage: string): Observable<boolean> {
    return this.dialog
      .open(DialogComponent, {
        data: {
          title: titleMessage,
          message: displayMessage,
        },
      })
      .afterClosed();
  }
}
