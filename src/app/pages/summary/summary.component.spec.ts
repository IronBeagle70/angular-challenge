import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { Passenger } from 'src/app/models/passenger.model';

import { PassengerComponent } from 'src/app/components/passenger/passenger.component';
import { PassengersComponent } from 'src/app/components/passengers/passengers.component';
import { SummaryComponent } from './summary.component';

import { BtnMessagePipe } from 'src/app/pipes/button-message/btn-message.pipe';
import { NumberMessagePipe } from 'src/app/pipes/number-message/number-message.pipe';

import { StoreService } from 'src/app/services/store/store.service';

import { of } from 'rxjs';

describe('SummaryComponent', () => {
  let component: SummaryComponent;
  let fixture: ComponentFixture<SummaryComponent>;
  let storeServiceSpy: jasmine.SpyObj<StoreService>;
  let el: DebugElement;

  beforeEach(async () => {
    const spy = jasmine.createSpyObj('StoreService', ['passengers$']);
    await TestBed.configureTestingModule({
      imports: [MatSnackBarModule, MatIconModule, MatDialogModule],
      declarations: [
        SummaryComponent,
        NumberMessagePipe,
        BtnMessagePipe,
        PassengersComponent,
        PassengerComponent,
      ],
      providers: [{ provide: StoreService, useValue: spy }],
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(SummaryComponent);
        storeServiceSpy = TestBed.inject(
          StoreService
        ) as jasmine.SpyObj<StoreService>;
        component = fixture.componentInstance;
        el = fixture.debugElement;
      });
  });

  it('summary component should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should display a message and button when there are no passengers', () => {
    storeServiceSpy.passengers$ = of([]);
    fixture.detectChanges();
    const passengerCount = el.query(By.css('.summary__indicator p'));
    const passengerCountBtn = el.query(By.css('.summary__indicator a'));

    expect(passengerCount.nativeElement.innerText).toBe(
      'Aún no hay pasajeros registrados'
    );
    expect(passengerCountBtn.nativeElement.innerText).toBe('Registrar');
  });

  it('should display correct passenger message and button', () => {
    const passengers: Passenger[] = [
      {
        names: 'test names 1',
        surnames: 'test surnames 1',
        nationality: 'test nationality 1',
        documentType: 'test documentType 1',
        documentNumber: 'test documentNumber 1',
      },
      {
        names: 'test names 2',
        surnames: 'test surnames 2',
        nationality: 'test nationality 2',
        documentType: 'test documentType 2',
        documentNumber: 'test documentNumber 2',
      },
    ];

    storeServiceSpy.passengers$ = of(passengers);

    fixture.detectChanges();

    const passengerCount = el.query(By.css('.summary__indicator p'));

    const passengerCountBtn = el.query(By.css('.summary__indicator a'));

    expect(passengerCount.nativeElement.innerText).toBe('Pasajeros 2 de 4');
    expect(passengerCountBtn.nativeElement.innerText).toBe(
      'Seguir Registrando'
    );
  });

  it('should display a message and button when there are 4 passengers', () => {
    const passengers: Passenger[] = [
      {
        names: 'test names 1',
        surnames: 'test surnames 1',
        nationality: 'test nationality 1',
        documentType: 'test documentType 1',
        documentNumber: 'test documentNumber 1',
      },
      {
        names: 'test names 2',
        surnames: 'test surnames 2',
        nationality: 'test nationality 2',
        documentType: 'test documentType 2',
        documentNumber: 'test documentNumber 2',
      },
      {
        names: 'test names 3',
        surnames: 'test surnames 3',
        nationality: 'test nationality 3',
        documentType: 'test documentType 3',
        documentNumber: 'test documentNumber 3',
      },
      {
        names: 'test names 4',
        surnames: 'test surnames 4',
        nationality: 'test nationality 4',
        documentType: 'test documentType 4',
        documentNumber: 'test documentNumber 4',
      },
    ];

    storeServiceSpy.passengers$ = of(passengers);

    fixture.detectChanges();

    const passengerCount = el.query(By.css('.summary__indicator p'));

    const passengerCountBtn = el.query(By.css('.summary__indicator a'));

    expect(passengerCount.nativeElement.innerText).toBe('Pasajeros 4 de 4');
    expect(passengerCountBtn).toBeNull();
  });

  it('should display passengers', () => {
    const passengers: Passenger[] = [
      {
        names: 'test names 1',
        surnames: 'test surnames 1',
        nationality: 'test nationality 1',
        documentType: 'test documentType 1',
        documentNumber: 'test documentNumber 1',
      },
      {
        names: 'test names 2',
        surnames: 'test surnames 2',
        nationality: 'test nationality 2',
        documentType: 'test documentType 2',
        documentNumber: 'test documentNumber 2',
      },
    ];

    storeServiceSpy.passengers$ = of(passengers);

    fixture.detectChanges();

    const passengersContainer = el.query(By.css('app-passengers'));
    const passengerContainer = el.queryAll(By.css('app-passenger'));

    expect(passengersContainer).toBeTruthy();
    expect(passengerContainer.length).toBe(2);
  });
});
