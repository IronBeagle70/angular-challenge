import { Component, OnInit } from '@angular/core';
import { StoreService } from 'src/app/services/store/store.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  passengersNumber = 0;

  constructor(private storeService: StoreService) {}

  ngOnInit(): void {
    this.storeService.passengers$.subscribe((passengers) => {
      this.passengersNumber = passengers.length;
    });
  }
}
